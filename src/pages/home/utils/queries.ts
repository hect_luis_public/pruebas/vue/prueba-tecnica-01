import gql from 'graphql-tag'
export const continentsQuery = gql`
  query {
    continents {
      code
      name
      countries {
        code
        name
        native
        phone
        capital
        currency
        emoji
        emojiU
      }
    }
  }
`
export const countriesQuery = gql`
  query {
    countries {
      code
      name
      native
      phone
      continent {
        code
        name
      }
      capital
      currency
      languages {
        code
        name
        native
        rtl
      }
      emoji
      emojiU
      states {
        code
        name
      }
    }
  }
`
