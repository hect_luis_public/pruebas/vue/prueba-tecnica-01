const API_KEY = import.meta.env.VITE_API_KEY
const API_URL = import.meta.env.VITE_API_IMAGES_URL

const fetchImage = async (text: string, type: string) => {
  const URL = `${API_URL}?key=${API_KEY}&q=${text}&image_type=${type}&page=1&per_page=3`
  const response = await fetch(URL)
  const data = await response.json()
  return data.hits[0]?.previewURL || ''
}

const fetchFlagImage = (name: string) => fetchImage(`${name}+flag+horizontal`, 'vector')
const fetchLandscapeImage = (name: string) => fetchImage(`${name}+city+capital`, 'photo')
const fetchContinentImage = (name: string) => fetchImage(`${name}+map`, 'vector')

export { fetchFlagImage, fetchLandscapeImage, fetchContinentImage }
