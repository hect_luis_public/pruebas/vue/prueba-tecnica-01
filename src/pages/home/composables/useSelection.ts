import { ref, type Ref } from 'vue'
import { type Continent } from '@/pages/home/interfaces/IContinentCountry'

const useSelection = (continentsWithImg: Ref<Continent[]>) => {
  const selectedContinents = ref<Continent[]>([])

  const changeSelection = (index: number) => {
    continentsWithImg.value[index].selected = !continentsWithImg.value[index].selected
    selectedContinents.value = continentsWithImg.value.filter((continent) => continent.selected)
  }

  const cleanFilter = () => {
    selectedContinents.value = []
    continentsWithImg.value.forEach((continent) => {
      continent.selected = false
    })
  }

  return { selectedContinents, changeSelection, cleanFilter }
}

export { useSelection }
