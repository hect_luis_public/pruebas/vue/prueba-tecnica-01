import { computed, type Ref } from 'vue'
import { type Country, type Continent } from '@/pages/home/interfaces/IContinentCountry'

const useFilter = (
  countriesWithFlag: Ref<Country[]>,
  searchFilter: Ref<string>,
  selectedContinents: Ref<Continent[]>
) => {
  const filterByContinents = () => {
    let filtered = countriesWithFlag.value
    if (searchFilter.value.length > 0) {
      filtered = filtered.filter(
        (country) => country.name.toLowerCase().includes(searchFilter.value.toLowerCase())
        /* 
          Se podría hacer más potente la búsqueda, pero creo basta con el name
          ||
          country.capital.toLowerCase().includes(searchFilter.value.toLowerCase()) ||
          country.continent.name.toLowerCase().includes(searchFilter.value.toLowerCase()) */
      )
    }
    if (selectedContinents.value.length > 0) {
      filtered = filtered.filter((country) => {
        return selectedContinents.value.some(
          (continent) => continent.code === country.continent.code
        )
      })
    }
    return filtered
  }
  const filteredCountries = computed(() => filterByContinents())
  return { filteredCountries }
}

export { useFilter }
