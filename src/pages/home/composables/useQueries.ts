import { ref, watch, computed } from 'vue'
import { useQuery } from '@vue/apollo-composable'
import { countriesQuery, continentsQuery } from '@/pages/home/utils/queries'
import { type Country, type Continent } from '@/pages/home/interfaces/IContinentCountry'

import {
  fetchContinentImage,
  fetchFlagImage,
  fetchLandscapeImage
} from '@/pages/home/utils/fetchImages'

const useQueries = () => {
  const countriesWithFlag = ref<Country[]>([])
  const continentsWithImg = ref<Continent[]>([])

  /* He optado por realizar únicamente dos consultas al inicio para obtener todos los países y continentes. 
  No consideré necesario hacerlo, por ejemplo, al ingresar en el buscador o al aplicar un filtro. 
  Estos son datos que no cambian con frecuencia, por lo que no es necesario mantenerlos actualizados constantemente */

  const { result: continentsResult } = useQuery(continentsQuery)
  const { result: countriesResult } = useQuery(countriesQuery)

  /* Se ha cortado la consulta de los paises para no hacer muchas peticiones al obtener la imagen de cada 
  continente y de cada pais con su respectiva bandera. Se ha utilizado la api de Pixabay */
  const countries = computed(() => countriesResult.value?.countries.slice(0, 10) || [])
  const continents = computed(() => continentsResult.value?.continents || [])

  watch(
    continents,
    async (newVal) => {
      if (newVal) {
        const continentsTemp = await Promise.all(
          newVal.map(async (continent: Continent) => {
            const img = await fetchContinentImage(continent.name)
            return { ...continent, img, selected: false }
          })
        )
        continentsWithImg.value = continentsTemp
      }
    },
    { immediate: true }
  )

  watch(
    countries,
    async (newVal) => {
      if (newVal) {
        const countriesTemp = await Promise.all(
          newVal.map(async (country: Country) => {
            const flag = await fetchFlagImage(country.name)
            const landscape = await fetchLandscapeImage(country.name)
            return { ...country, flag, landscape }
          })
        )
        countriesWithFlag.value = countriesTemp
      }
    },
    { immediate: true }
  )

  return { countriesWithFlag, continentsWithImg, countriesResult, continentsResult }
}

export { useQueries }
