import { type Country } from '@/pages/home/interfaces/IContinentCountry'

export const defaultCountry: Country = {
  code: '',
  name: '',
  continent: { code: '', name: '' },
  capital: '',
  currency: '',
  languages: [{ code: '', name: '' }]
}
