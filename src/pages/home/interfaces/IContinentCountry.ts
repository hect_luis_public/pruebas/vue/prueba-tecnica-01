interface Language {
  code: string
  name: string
}

interface ContinentBase {
  code: string
  name: string
}

export interface Country {
  code: string
  name: string
  continent: ContinentBase
  capital: string
  currency: string
  languages: Language[]
  landscape?: string
  flag?: string
}

export interface Continent extends ContinentBase {
  countries: Country[]
  selected?: boolean
  img?: string
}
