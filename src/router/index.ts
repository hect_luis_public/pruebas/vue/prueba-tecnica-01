import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/pages/home/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/vista1',
      name: 'vista1',
      component: () => import('@/pages/view1/View1.vue')
    },
    {
      path: '/vista2',
      name: 'vista2',
      component: () => import('@/pages/view2/View2.vue')
    }
  ]
})

export default router
